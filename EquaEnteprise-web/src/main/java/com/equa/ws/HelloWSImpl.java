/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equa.ws;

import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author User
 */
@WebService
public class HelloWSImpl implements HelloWS  {

    @Override
    @WebMethod
    public String sayHello(String name) {
        return "Ahoj" + name;
    }
    
}
